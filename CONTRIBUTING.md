## Contibution Guide

This project is written in JavaScript and Developed for Android using the [DroidScript IDE][1]...  
You may contribute to this project in the following way.

- Run the app on your device and report any bugs.
   - Install [DroidScript][1] on your Android Device 
   - Download [the SPK file](spk/pwnDroid.spk) from the repo and add it to DroidScript
   - Run the project and test the features
   - If you find a bug [Open an Issue](https://gitlab.com/backroom-studio/pwnDroid/issues/new) to let us know

- Modify the code to fix bugs or add features
   - Create a [New Branch](https://gitlab.com/backroom-studio/pwnDroid/branches/new) of the project 
   - Use DroidScript to edit the code and test your changes
   - Commit and Push your code to the new branch
   - Submit a [Merge Request](https://gitlab.com/backroom-studio/pwnDroid/merge_requests/new)

If you make a merge request please include an updated spk in the request.

[1]: http://droidscript.org