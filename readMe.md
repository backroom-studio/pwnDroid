What is pwnDroid

pwnDroid provides access to a huge collection of pen-testing tools in apk format that have either been developed specially for Android or ported to Android from pre-existing Linux utilities... pwnDroid also aims to provide tutorials detailing when and how each tool can (and should) be used...   [Are you developing a Pen-testing utility for Android?  Consider adding it to pwnDroid. We encourage users to support the tools they use by providing a donation link for each utility in the collection]

Get involved

pwnDroid is written in JavaScript and Developed on Android using the DroidScript IDE (available for free in the PlayStore)... If you want to contribute to pwnDroid, after creating a branch you will need to use the included SPK file to add the project to your own copy of DroidScript. Please include an updated SPK file with each pull request.